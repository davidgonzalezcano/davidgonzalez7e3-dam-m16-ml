# %matplotlib inline
import pandas as pd
import matplotlib.pyplot as plt
import pylab as pl
import seaborn as sns

from pandas import scatter_matrix
from matplotlib import cm

fruits = pd.read_table('fruites.txt')

# mostrar capcaleres
print(fruits.head())

# mostrar cuantes fruites hi ha i el número de caracteristiques que tenen (columnes)
print(fruits.shape)

# mostrar els tipus de fruites
print(fruits['fruit_name'].unique())

# mostra el recompte de fruites per el nom
print(fruits.groupby('fruit_name').size())

#mostra un grafic amb el recompte de fruites per nom
sns.countplot(fruits['fruit_name'],label="Count")
plt.show()

#Mostra 4 grafics amb les caracteristiques
fruits.drop('fruit_label', axis=1).plot(
    kind='box', subplots=True, layout=(2,2),
    sharex=False, sharey=False, figsize=(9,9),
    title='Box Plot for each input variable')
plt.savefig('fruits_box')
plt.show()

# mostra un grafic de barres amb les caracteristiques
fruits.drop('fruit_label' ,axis=1).hist(bins=30, figsize=(9,9))
pl.suptitle("Histogram for each numeric input variable")
plt.savefig('fruits_hist')
plt.show()


feature_names = ['mass', 'width', 'height', 'color_score']
X = fruits[feature_names]
y = fruits['fruit_label']
cmap = cm.get_cmap('gnuplot')
#scatter = scatter_matrix(X, c = y, marker = 'o', s=40, hist_kwds={'bins':15}, figsize=(9,9), cmap = cmap)
#plt.suptitle('Scatter-matrix for each input variable')
#plt.savefig('fruits_scatter_matrix')

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)
