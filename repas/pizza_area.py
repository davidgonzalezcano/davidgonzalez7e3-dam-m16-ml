import math

diametre = float(input("Diametre pizza: "))

# S = pi * r^2
radi = diametre / 2
superficie = math.pi * radi**2

print("Superficie: {:.2f}".format(superficie))
