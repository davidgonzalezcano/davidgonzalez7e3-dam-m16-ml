def max_llista(llista):
    maxim = llista[0]
    for x in llista:
        if x > maxim:
            maxim = x

    return maxim

entrada = input()
llista = [int(x) for x in entrada.split(",")]

maxim = max_llista(llista)
print(maxim)