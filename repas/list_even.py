entrada = input()

llista = [int(x) for x in entrada.split(",")]

even_list = []
for element in llista:
    if element % 2 == 0:
        even_list.append(element)

even_list2 = [x for x in llista if x % 2 == 0]

print(even_list)
print(even_list2)