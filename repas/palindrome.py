def es_palindrom(paraula):
    i = 0
    j = len(paraula) -1

    while i <= j:
        if paraula[i] != paraula[j]:
            return False
        i += 1
        j -= 1
    return True

entrada = input()
palindrom = es_palindrom(entrada)
print(palindrom)