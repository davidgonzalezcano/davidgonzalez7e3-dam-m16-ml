n = int(input("Introdueix el valor: "))

factorial = 1
current = 2
while current <= n:
    factorial *= current
    current += 1

print(factorial)