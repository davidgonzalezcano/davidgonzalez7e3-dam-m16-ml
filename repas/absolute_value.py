
n = int(input("Introdueix el valor: "))

print("Amb abs(): " + str(abs(n)))

if n < 0:
    n = -n

print("Sense abs(): " + str(n))
