import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-2, 2, 10)

# Note that even in the OO-style, we use `.pyplot.figure` to create the figure.
plt.plot(x, x, "-", label='Linial')  # Plot some data on the axes.
plt.plot(x, x**2, "o", label='Quadràtica')  # Plot more data on the axes...
plt.plot(x, x**3, ".", label='Cúbica')  # ... and some more.
plt.xlabel('Valors de X')  # Add an x-label to the axes.
plt.ylabel('Valors de Y')  # Add a y-label to the axes.
plt.title("Funcions")  # Add a title to the axes.
plt.legend()  # Add a legend.
plt.show()
