import matplotlib.pyplot as plt
import numpy as np

x = np.random.randint(20, size=1000)
print(x)

plt.hist(x, bins=20) # Plot some data on the axes.
plt.show()
