import pandas as pd
import re
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures

from urllib.request import Request, urlopen  # Python 3

url = "https://gitlab.com/joan.puigcerver/joanpuigcerver-dam-m16-ml/-/raw/master/data/laptops.csv"

# Aquesta part s'utilitza per poder accedir a la URL de GitLab (no accepta connexions que no vinguin de un browser)
req = Request(url)
req.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0')
content = urlopen(req)

laptops = pd.read_csv(content, index_col=0, encoding='utf-16')
print(laptops)

def extract_ram(x):
    match = re.search(r"(\d+)GB", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Error al extreure la Ram", x)


def extract_cpu(x):
    match = re.search(r"(\d+(\.\d+)?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("Error al extreure la Cpu", x)


def extract_resolution(x):
    match = re.search(r"(\d+)(x\d+)", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Error al extreure la ScreenResolution", x)

def extract_weight(x):
    match = re.search(r"(\d+(\.\d+)?)kg", x)
    if match:
        return float(match.groups()[0])

    else:
        raise Exception("Error al extreure el Weight", x)

laptops["Ram"] = laptops["Ram"].apply(extract_ram)
laptops["Cpu"] = laptops["Cpu"].apply(extract_cpu)
laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(extract_resolution)
laptops["Weight"] = laptops["Weight"].apply(extract_weight)

# print(laptops["Ram"])
# print(laptops["Cpu"])
# print(laptops["ScreenResolution"])
# print(laptops["Weight"])

def regressioLineal(laptops_features):
    X = laptops.loc[:, laptops_features]
    y = laptops["Price_euros"]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2020)
    regression_model = LinearRegression()
    regression_model.fit(X_train, y_train)
    print(str(" Regressió lineal ") + str(laptops_features))
    print(str(" train: ") + str(regression_model.score(X_train, y_train)))
    print(str(" test:  ") + str(regression_model.score(X_test, y_test)))


def regressioPolinomial(laptops_features):
    X = laptops.loc[:, laptops_features]
    y = laptops["Price_euros"]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2020)
    degree = 2
    regression_model = make_pipeline(PolynomialFeatures(degree), LinearRegression())
    regression_model.fit(X_train, y_train)
    print(str(" Regressió polinomial ") + str(laptops_features))
    print(str(" train: ") + str(regression_model.score(X_train, y_train)))
    print(str(" test:  ") + str(regression_model.score(X_test, y_test)))

print("-------------------------RAM--------------------------")
lineal_ram = regressioLineal(["Ram"])
polynomial_ram = regressioPolinomial(["Ram"])
print()
print("-------------------------CPU--------------------------")
lineal_cpu = regressioLineal(["Cpu"])
polynomial_cpu = regressioPolinomial(["Cpu"])
print()
print("-----------------------CPU-RAM------------------------")
lineal_cpu_ram = regressioLineal(["Cpu", "Ram"])
polynomial_cpu_ram = regressioPolinomial(["Cpu", "Ram"])
print()
print("---------------CPU-RAM-ScreenResolution---------------")
lineal_cpu_ram_screen = regressioLineal(["Cpu", "Ram", "ScreenResolution"])
polynomial_cpu_ram_screen = regressioPolinomial(["Cpu", "Ram", "ScreenResolution"])
print()
print("------------CPU-RAM-ScreenResolution-Weight-------------")
lineal_cpu_ram_screen_weight = regressioLineal(["Cpu", "Ram", "ScreenResolution", "Weight"])
polynomial_cpu_ram_screen_weight = regressioPolinomial(["Cpu", "Ram", "ScreenResolution", "Weight"])
print()
