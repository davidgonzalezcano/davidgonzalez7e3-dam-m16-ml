import pandas as pd
import matplotlib.pyplot as plt
data = pd.read_csv("../data/titanic.csv")

#Comandes (Run FIle Python Console)

# ex1: Mostrar la mida del dataset (nombre de persones (mostres) i nombre de característiques)
print(data.shape)

#ex2: Mostrar el identificador de les columnes.
columns = data.columns
print(columns)

#ex3: Obtenir els 5 primers passatgers
head = data.head()
print(head)

#ex4: Obtenir el Nom, Sexe i Edat dels 5 primers passatgers
fivePassengers = data[["Name", "Sex", "Age"]].head()
print(fivePassengers)

#ex5: Obtenir el nombre de persones amb menys de 30 anys.
below_30 = data[data["Age"] < 30].shape
print(below_30)

#ex6: Obtenir el Nom, Sexe i Edat de les persones amb menys de 30 anys.
print(data[data["Age"] < 30] [["Name", "Sex", "Age"]])
print(data.loc[data["Age"] < 30, ["Name", "Sex", "Age"]])

#ex7 edat de persona mes jove
print(data["Age"].min())

#ex8 nom de la persona mes gran
max_edat = data["Age"].max()
print(data.loc[data["Age"] == max_edat, "Name"])

#ex9 mitja dels que han sobreviscut
survived = data[data["Survived"] == 1]
print(survived["Age"].mean())

#ex10

#data = pd.read_csv()
#data_female =
#data_male =
