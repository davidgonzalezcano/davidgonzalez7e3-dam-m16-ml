import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import re


data = pd.read_csv("../data/laptops.csv", encoding="UTF-16")

def extract_ram(x):
    match = re.search(r"(\d+)GB", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Error al extreure la Ram", x)

def extract_cpu(x):
    match = re.search(r"(\d+(\.\d+)?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("Error al extreure la Cpu", x)

def extract_resolution(x):
    match = re.search(r"(\d+)(x\d+)", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Error al extreure la ScreenResolution", x)

def extract_weight(x):
    match = re.search(r"(\d+(\.\d+)?)kg", x)
    if match:
        return float(match.groups()[0])

    else:
        raise Exception("Error al extreure el Weight", x)


# Realitzar preprocessament si escau
data["Ram"] = data["Ram"].apply(extract_ram)
data["Cpu"] = data["Cpu"].apply(extract_cpu)
data["ScreenResolution"] = data["ScreenResolution"].apply(extract_resolution)
data["Weight"] = data["Weight"].apply(extract_weight)

x_features = ["Ram", "Cpu", "ScreenResolution", "Weight"]
y_feature = "Price_euros"

X = data.loc[:, x_features]
y = data[y_feature]


X_train = X.sample(frac=0.8, random_state=2021)
X_test = X.drop(X_train.index)

y_train = y[X_train.index]
y_test = y[X_test.index]

normalizer = keras.layers.experimental.preprocessing.Normalization()
normalizer.adapt(np.array(X_train))

model = keras.models.Sequential()
# Capa d'entrada
model.add(normalizer)

# Capes ocultes
model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
#model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
#model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
#model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
#model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
#model.add(tf.keras.layers.Dropout(0.2))

# Capa de sortida
model.add(tf.keras.layers.Dense(1)) # Regressió

# Regressió
model.compile(optimizer=tf.optimizers.Adam(learning_rate=1e-4),
              loss="mean_squared_error",
              metrics=tfa.metrics.RSquare(y_shape=(1,)))

# Mètode per imprimir una gràfica amb l'error del entrenament
def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss')
    plt.yscale('log')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()

history = model.fit(X_train, y_train, epochs=5000, validation_split=0.2)
plot_loss(history)

loss, r2 = model.evaluate(X_test, y_test)
print("rsquared: ", r2)
