import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import re


data = pd.read_csv("../data/iris.csv")

# Realitzar preprocessament si escau

x_features = ["sepal_length", "sepal_width", "petal_length", "petal_width"]
y_feature = "species"

data['species'] = data['species'].astype('category')
data['species'] = data['species'].cat.codes

X = data.loc[:, x_features]
y = data[y_feature]

X_train = X.sample(frac=0.8, random_state=2021)
X_test = X.drop(X_train.index)

y_train = y[X_train.index]
y_test = y[X_test.index]

normalizer = keras.layers.experimental.preprocessing.Normalization()
normalizer.adapt(np.array(X_train))

model = keras.models.Sequential()
# Capa d'entrada
model.add(normalizer)

# Capes ocultes
model.add(tf.keras.layers.Dense(16, activation=tf.nn.relu))
model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(8, activation=tf.nn.relu))
# Capa de sortida
model.add(tf.keras.layers.Dense(3, activation=tf.nn.softmax)) # Classificació

# Classificació
model.compile(optimizer=tf.optimizers.Adam(learning_rate=1e-3),
              loss="sparse_categorical_crossentropy",
              metrics=['accuracy'])

# Mètode per imprimir una gràfica amb l'error del entrenament
def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss')
    plt.yscale('log')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()

history = model.fit(X_train, y_train, epochs=1000, validation_split=0.2)
plot_loss(history)
loss, metric = model.evaluate(X_test, y_test)
print("loss: ", loss)
print("accuracy: ", metric)
